## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
mvn compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Provided Code

### RESTEasy JAX-RS

unprotected resource:
```shell script
curl -v "localhost:8080/hello"
```

protected resource, require **_batman_** role to access:
```shell script
curl -v -H "Authorization: Bearer <access_token>" "localhost:8080/protected"
```

### JWT generation
to generate access token:
```shell script
mvn exec:java -Dexec.mainClass=com.opswat.jwt.GenerateToken -Dsmallrye.jwt.sign.key.location=privateKey.pem
```
