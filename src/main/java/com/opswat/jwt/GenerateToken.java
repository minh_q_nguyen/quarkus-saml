package com.opswat.jwt;

import org.eclipse.microprofile.jwt.Claims;

import java.util.Arrays;
import java.util.HashSet;

import io.smallrye.jwt.build.Jwt;

public class GenerateToken {

    public static void main(String[] args) {
        String token = Jwt.issuer("https://www.web.com/issuer")
                .upn("batman@batcave.com")
                .groups(new HashSet<>(Arrays.asList("batmans")))
                .claim(Claims.birthdate.name(), "1970-01-01")
                .sign();
        System.out.println(token);
    }

}
