package com.opswat.resources;

import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

@Path("/protected")
@RequestScoped
public class ProtectedResource {

    @Inject
    private JsonWebToken jwt;

    @GET
    @RolesAllowed("batmans")
    @Produces(MediaType.TEXT_PLAIN)
    public String getProtectedResource(@Context SecurityContext ctx) {
        String userPricipal;
        if (ctx.getUserPrincipal() == null) {
            userPricipal = "anonym";
        }
        else if (!ctx.getUserPrincipal().getName().equals(jwt.getName())) {
            throw new InternalServerErrorException("principal and jwt names do not match!!");
        }
        else {
            userPricipal = ctx.getUserPrincipal().getName();
        }
        return String.format("%s! this is protected resource", userPricipal);
    }

}
